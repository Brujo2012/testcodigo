//
//  DetailViewController.swift
//  TestCodigo
//
//  Created by Mario Alejandro Ramos on 15-02-19.
//  Copyright © 2019 Mario Alejandro Ramos. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var portadaUrl = ""
    var titleText = ""
    var overviewText = ""
    var releaseDateText = ""
    var votesText = ""
    
    @IBOutlet weak var portada: UIImageView!
    @IBOutlet weak var titulo: UILabel!
    
    @IBOutlet weak var votesLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var overviewTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadData()
    }
    
    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(from url: URL) {
        print("Download Started for image \(url.absoluteString)")
        
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() {
                self.portada.image = UIImage(data: data)
            }
        }
    }


    func loadData(){
        self.titulo.text! = titleText
        self.overviewTextView.text = overviewText
        self.releaseDateLabel.text = releaseDateText
        self.votesLabel.text = votesText
        let finalURL = "http://image.tmdb.org/t/p/w500/" + portadaUrl
        
        if let url = URL(string:finalURL) {
            portada.contentMode = .scaleAspectFit
            downloadImage(from: url)
        }
    
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
