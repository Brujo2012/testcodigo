//
//  SecondViewController.swift
//  TestCodigo
//
//  Created by Mario Alejandro Ramos on 15-02-19.
//  Copyright © 2019 Mario Alejandro Ramos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SecondViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tblJSON1: UITableView!
     var moviesList : [Movie] = []
    var arrRes = [[String:AnyObject]]() //Array of dictionary
    
    let urlPopular = "https://api.themoviedb.org/3/movie/popular?api_key=34738023d27013e6d1b995443764da44"
    override func viewDidLoad() {
        super.viewDidLoad()
        Alamofire.request(urlPopular).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                
                if let resData = swiftyJsonVar["results"].arrayObject {
                    self.arrRes = resData as! [[String:AnyObject]]
                }
                if self.arrRes.count > 0 {
                    self.tblJSON1.reloadData()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "jsonCell")!
        var dict = arrRes[(indexPath as NSIndexPath).row]
        cell.textLabel?.text = dict["title"] as? String
        let movie = Movie()
        movie.movieTitle = (dict["title"] as? String)!
        movie.moviePopularity = (dict["popularity"] as! NSNumber).stringValue
        movie.moviePosterURL = (dict["poster_path"] as? String)!
        movie.movieOverview = (dict["overview"] as? String)!
        movie.movieVoteAvg = (dict["vote_average"] as! NSNumber).stringValue
        movie.movieDate = (dict["release_date"] as? String)!
        moviesList.append(movie)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRes.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailViewController(nibName: "DetailViewController", bundle: nil)
        
            let movie : Movie = moviesList[indexPath.row]
            vc.titleText = movie.movieTitle
            vc.portadaUrl = movie.moviePosterURL
            vc.overviewText = movie.movieOverview
            vc.releaseDateText = movie.movieDate
            vc.votesText = movie.movieVoteAvg
            vc.modalPresentationStyle = UIModalPresentationStyle.popover
            
            self.present(vc, animated: true)
 
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

