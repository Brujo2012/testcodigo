//
//  movie.swift
//  TestCodigo
//
//  Created by Mario Alejandro Ramos on 16-02-19.
//  Copyright © 2019 Mario Alejandro Ramos. All rights reserved.
//

import Foundation

class Movie: NSObject {
    
    var movieTitle = ""
    var moviePopularity = ""
    var moviePosterURL = ""
    var movieOverview = ""
    var movieVoteAvg = ""
    var movieDate = ""
    
    override init() {
        
    }

}
